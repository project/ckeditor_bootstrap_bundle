A plugin manager for CKEditor bootstrap Bundle plugins.

This module is based on the paid for bootstrap plugins by Michael Janea,
it's a small one off payment. To purchase go to the link below and buy the bootstrap bundle.

http://www.michaeljanea.com/ckeditor/

- Download plugins to /plugins

- Install the module bootstrap Bundle and ckeditor at /admin/modules

- The plugins show up in the CKEditor configuration section
(admin/config/content/ckeditor/edit/Full (or Advanced).

- In the CKEditor profile under 'Editor appearance' move the icon into the toolbar
and check the plugins in 'Plugins' section.
